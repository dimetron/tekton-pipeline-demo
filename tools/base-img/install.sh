#!/bin/bash

export SDKMAN_DIR="/root/.sdkman"

rm -rf /root/.sdkman
curl -s https://get.sdkman.io | bash

echo "sdkman_auto_answer=true" > $SDKMAN_DIR/etc/config
echo "sdkman_auto_selfupdate=false" >> $SDKMAN_DIR/etc/config
echo "sdkman_insecure_ssl=true" >> $SDKMAN_DIR/etc/config

#install maven and java 8
zsh -c 'set +x;source /root/.sdkman/bin/sdkman-init.sh'
zsh -c 'source "/root/.sdkman/bin/sdkman-init.sh" && sdk install gradle'
zsh -c 'source "/root/.sdkman/bin/sdkman-init.sh" && sdk ls java && sdk install java 8.0.265-amzn'
zsh -c 'chmod +x /root/.sdkman/candidates/java/current/bin/java'

#install kind kubectl
wget -O /usr/local/bin/kind https://github.com/kubernetes-sigs/kind/releases/download/${KIND}/kind-linux-amd64
chmod +x /usr/local/bin/kind
wget -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/${KUBECTL}/bin/linux/amd64/kubectl
chmod +x /usr/local/bin/kubectl

#tekton
curl -sLO "https://github.com/tektoncd/cli/releases/download/v0.11.0/tkn_0.11.0_Linux_x86_64.tar.gz" && \
tar xvzf tkn_0.11.0_Linux_x86_64.tar.gz -C /usr/bin tkn && \
chmod +x /usr/bin/tkn

#kapp
curl -sLO "https://github.com/k14s/kapp/releases/download/v0.33.0/kapp-linux-amd64" && \
mv kapp-linux-amd64 /usr/bin/kapp && \
chmod +x /usr/bin/kapp

echo "Done."